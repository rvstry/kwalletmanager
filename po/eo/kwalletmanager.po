# Translation of kwalletmanager into esperanto.
# Axel Rousseau <axel@esperanto-jeunes.org>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: kwalletmanager\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-22 00:51+0000\n"
"PO-Revision-Date: 2009-11-15 12:06+0100\n"
"Last-Translator: Axel Rousseau <axel@esperanto-jeunes.org>\n"
"Language-Team: esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: pology\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Axel Rousseau"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "axel@esperanto-jeunes.org"

#: allyourbase.cpp:203
#, kde-format
msgid "An entry by the name '%1' already exists. Would you like to continue?"
msgstr ""

#: allyourbase.cpp:229 allyourbase.cpp:234
#, kde-format
msgid "A folder by the name '%1' already exists.  What would you like to do?"
msgstr ""

#: allyourbase.cpp:232 allyourbase.cpp:234
#, kde-format
msgid "Replace"
msgstr "Anstataŭigi"

#: allyourbase.cpp:349
#, kde-format
msgid "Folders"
msgstr "Dosierujoj"

#: allyourbase.cpp:383
#, kde-format
msgid "An unexpected error occurred trying to drop the item"
msgstr ""

#: allyourbase.cpp:459
#, kde-format
msgid "An unexpected error occurred trying to drop the entry"
msgstr ""

#: allyourbase.cpp:483
#, kde-format
msgid ""
"An unexpected error occurred trying to delete the original folder, but the "
"folder has been copied successfully"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: applicationsmanager.ui:17
#, kde-format
msgid "These applications are currently connected to this wallet:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: applicationsmanager.ui:64
#, kde-format
msgid "These applications are authorized to access this wallet:"
msgstr ""

#: disconnectappbutton.cpp:16
#, kde-format
msgid "Disconnect"
msgstr "Malkonekti"

#. i18n: ectx: property (text), widget (QPushButton, _allowOnce)
#: kbetterthankdialogbase.ui:43
#, kde-format
msgid "&Replace"
msgstr "&Anstataŭigi"

#. i18n: ectx: property (text), widget (QPushButton, _allowAlways)
#: kbetterthankdialogbase.ui:53
#, fuzzy, kde-format
#| msgid "Replace &all"
msgid "Replace &All"
msgstr "&Anstataŭigi ĉiujn"

#. i18n: ectx: property (text), widget (QPushButton, _deny)
#: kbetterthankdialogbase.ui:60
#, kde-format
msgid "&Skip"
msgstr "La&si"

#. i18n: ectx: property (text), widget (QPushButton, _denyForever)
#: kbetterthankdialogbase.ui:67
#, kde-format
msgid "Skip A&ll"
msgstr ""

#: kwalleteditor.cpp:87
#, kde-format
msgid "Search"
msgstr "Serĉo"

#: kwalleteditor.cpp:96
#, kde-format
msgid "&Show values"
msgstr ""

#: kwalleteditor.cpp:196
#, kde-format
msgid "&New Folder..."
msgstr "&Nova dosierujo..."

#: kwalleteditor.cpp:200
#, kde-format
msgid "&Delete Folder"
msgstr "&Forviŝu dosierujon"

#: kwalleteditor.cpp:203
#, fuzzy, kde-format
#| msgid "&New Folder..."
msgid "&Import a wallet..."
msgstr "&Nova dosierujo..."

#: kwalleteditor.cpp:206
#, fuzzy, kde-format
#| msgid "&Export..."
msgid "&Import XML..."
msgstr "E&ksportu..."

#: kwalleteditor.cpp:209
#, fuzzy, kde-format
#| msgid "&Export..."
msgid "&Export as XML..."
msgstr "E&ksportu..."

#: kwalleteditor.cpp:212
#, kde-format
msgid "&Copy"
msgstr ""

#: kwalleteditor.cpp:217
#, kde-format
msgid "&New..."
msgstr "&Nova..."

#: kwalleteditor.cpp:222
#, kde-format
msgid "&Rename"
msgstr "&Renomi"

#: kwalleteditor.cpp:227 kwalletpopup.cpp:65
#, kde-format
msgid "&Delete"
msgstr "&Forigi"

#: kwalleteditor.cpp:232
#, kde-format
msgid "Always show contents"
msgstr ""

#: kwalleteditor.cpp:236
#, kde-format
msgid "Always hide contents"
msgstr ""

#: kwalleteditor.cpp:345
#, kde-format
msgid "Passwords"
msgstr "Pasvortoj"

#: kwalleteditor.cpp:346
#, kde-format
msgid "Maps"
msgstr "Mapoj"

#: kwalleteditor.cpp:347
#, kde-format
msgid "Binary Data"
msgstr ""

#: kwalleteditor.cpp:348
#, kde-format
msgid "Unknown"
msgstr "Nekonata"

#: kwalleteditor.cpp:388
#, kde-format
msgid "Are you sure you wish to delete the folder '%1' from the wallet?"
msgstr ""

#: kwalleteditor.cpp:392
#, kde-format
msgid "Error deleting folder."
msgstr ""

#: kwalleteditor.cpp:411
#, kde-format
msgid "New Folder"
msgstr "Nova dosierujo"

#: kwalleteditor.cpp:412
#, kde-format
msgid "Please choose a name for the new folder:"
msgstr ""

#: kwalleteditor.cpp:423 kwalleteditor.cpp:428
#, kde-format
msgid "Sorry, that folder name is in use. Try again?"
msgstr ""

#: kwalleteditor.cpp:425 kwalleteditor.cpp:428 kwalleteditor.cpp:803
#: kwalleteditor.cpp:806
#, kde-format
msgid "Try Again"
msgstr ""

#: kwalleteditor.cpp:426 kwalleteditor.cpp:428 kwalleteditor.cpp:804
#: kwalleteditor.cpp:806 kwalletmanager.cpp:379 kwalletmanager.cpp:381
#, kde-format
msgid "Do Not Try"
msgstr ""

#: kwalleteditor.cpp:472
#, kde-format
msgid "Error saving entry. Error code: %1"
msgstr ""

#: kwalleteditor.cpp:499
#, kde-format
msgid ""
"The contents of the current item has changed.\n"
"Do you want to save changes?"
msgstr ""

#: kwalleteditor.cpp:553 kwalleteditor.cpp:893
#, fuzzy, kde-format
#| msgid "Passwords"
msgid "Password: %1"
msgstr "Pasvortoj"

#: kwalleteditor.cpp:570 kwalleteditor.cpp:895
#, kde-format
msgid "Name-Value Map: %1"
msgstr ""

#: kwalleteditor.cpp:580 kwalleteditor.cpp:897
#, kde-format
msgid "Binary Data: %1"
msgstr ""

#: kwalleteditor.cpp:788
#, kde-format
msgid "New Entry"
msgstr ""

#: kwalleteditor.cpp:789
#, kde-format
msgid "Please choose a name for the new entry:"
msgstr ""

#: kwalleteditor.cpp:801 kwalleteditor.cpp:806
#, kde-format
msgid "Sorry, that entry already exists. Try again?"
msgstr ""

#: kwalleteditor.cpp:828 kwalleteditor.cpp:837
#, kde-format
msgid "An unexpected error occurred trying to add the new entry"
msgstr ""

#: kwalleteditor.cpp:889
#, kde-format
msgid "An unexpected error occurred trying to rename the entry"
msgstr ""

#: kwalleteditor.cpp:909
#, kde-format
msgid "Are you sure you wish to delete the item '%1'?"
msgstr ""

#: kwalleteditor.cpp:913
#, kde-format
msgid "An unexpected error occurred trying to delete the entry"
msgstr ""

#: kwalleteditor.cpp:941
#, kde-format
msgid "Unable to open the requested wallet."
msgstr ""

#: kwalleteditor.cpp:977
#, kde-format
msgid "Unable to create temporary file for downloading '<b>%1</b>'."
msgstr ""

#: kwalleteditor.cpp:984
#, kde-format
msgid "Unable to access wallet '<b>%1</b>'."
msgstr ""

#: kwalleteditor.cpp:1015 kwalleteditor.cpp:1045 kwalleteditor.cpp:1079
#: kwalleteditor.cpp:1161
#, kde-format
msgid ""
"Folder '<b>%1</b>' already contains an entry '<b>%2</b>'.  Do you wish to "
"replace it?"
msgstr ""

#: kwalleteditor.cpp:1119
#, kde-format
msgid "Unable to access XML file '<b>%1</b>'."
msgstr ""

#: kwalleteditor.cpp:1125
#, kde-format
msgid "Error reading XML file '<b>%1</b>' for input."
msgstr ""

#: kwalleteditor.cpp:1131
#, kde-format
msgid "Error: XML file does not contain a wallet."
msgstr ""

#: kwalleteditor.cpp:1282
#, kde-format
msgid "Unable to store to '<b>%1</b>'."
msgstr ""

#: kwalletmanager.cpp:73
#, kde-format
msgid ""
"The KDE Wallet system is not enabled. Do you want me to enable it? If not, "
"the KWalletManager will quit as it cannot work without reading the wallets."
msgstr ""

#: kwalletmanager.cpp:101 kwalletmanager.cpp:108 kwalletmanager.cpp:229
#: kwalletmanager.cpp:302
#, fuzzy, kde-format
#| msgid "KDE Wallet"
msgid "Wallet"
msgstr "KDE paperujo"

#: kwalletmanager.cpp:101 kwalletmanager.cpp:302
#, kde-format
msgid "No wallets open."
msgstr ""

#: kwalletmanager.cpp:108 kwalletmanager.cpp:229
#, kde-format
msgid "A wallet is open."
msgstr ""

#: kwalletmanager.cpp:146 kwalletpopup.cpp:27
#, fuzzy, kde-format
#| msgid "&New Folder..."
msgid "&New Wallet..."
msgstr "&Nova dosierujo..."

#: kwalletmanager.cpp:151
#, fuzzy, kde-format
#| msgid "&Delete Folder"
msgid "&Delete Wallet..."
msgstr "&Forviŝu dosierujon"

#: kwalletmanager.cpp:156
#, kde-format
msgid "Export as encrypted"
msgstr ""

#: kwalletmanager.cpp:161
#, kde-format
msgid "&Import encrypted"
msgstr ""

#: kwalletmanager.cpp:166
#, kde-format
msgid "Configure &Wallet..."
msgstr ""

#: kwalletmanager.cpp:174
#, kde-format
msgid "Close &All Wallets"
msgstr ""

#: kwalletmanager.cpp:264 kwalletmanager.cpp:269 walletcontrolwidget.cpp:123
#: walletcontrolwidget.cpp:128
#, kde-format
msgid ""
"Unable to close wallet cleanly. It is probably in use by other applications. "
"Do you wish to force it closed?"
msgstr ""

#: kwalletmanager.cpp:266 kwalletmanager.cpp:269 walletcontrolwidget.cpp:125
#: walletcontrolwidget.cpp:128
#, kde-format
msgid "Force Closure"
msgstr ""

#: kwalletmanager.cpp:267 kwalletmanager.cpp:269 walletcontrolwidget.cpp:126
#: walletcontrolwidget.cpp:128
#, kde-format
msgid "Do Not Force"
msgstr ""

#: kwalletmanager.cpp:278 walletcontrolwidget.cpp:137
#, kde-format
msgid "Unable to force the wallet closed. Error code was %1."
msgstr ""

#: kwalletmanager.cpp:294
#, kde-format
msgid "Error opening wallet %1."
msgstr ""

#: kwalletmanager.cpp:336
#, kde-format
msgid "Please choose a name for the new wallet:"
msgstr ""

#: kwalletmanager.cpp:347
#, fuzzy, kde-format
#| msgid "New Folder"
#| msgid_plural "New Folder (%1)"
msgid "New Wallet"
msgstr "Nova dosierujo"

#: kwalletmanager.cpp:369
#, kde-format
msgid "Empty name is not supported. Please select a new one."
msgstr ""

#: kwalletmanager.cpp:369
#, kde-format
msgid "Create new wallet"
msgstr ""

#: kwalletmanager.cpp:376 kwalletmanager.cpp:381
#, kde-format
msgid "Sorry, that wallet already exists. Try a new name?"
msgstr ""

#: kwalletmanager.cpp:378 kwalletmanager.cpp:381
#, kde-format
msgid "Try New"
msgstr ""

#: kwalletmanager.cpp:411
#, kde-format
msgid "Are you sure you wish to delete the wallet '%1'?"
msgstr ""

#: kwalletmanager.cpp:417
#, kde-format
msgid "Unable to delete the wallet. Error code was %1."
msgstr ""

#: kwalletmanager.cpp:465
#, fuzzy, kde-format
#| msgid "&Rename"
msgid "File name"
msgstr "&Renomi"

#: kwalletmanager.cpp:472
#, kde-format
msgid "Failed to open file for writing"
msgstr ""

#: kwalletmanager.cpp:483
#, kde-format
msgid "Select file"
msgstr ""

#: kwalletmanager.cpp:493
#, kde-format
msgid "Failed to open file"
msgstr ""

#: kwalletmanager.cpp:506
#, kde-format
msgid "Wallet named %1 already exists, Operation aborted"
msgstr ""

#: kwalletmanager.cpp:513
#, kde-format
msgid "Failed to copy files"
msgstr ""

#: kwalletmanager.cpp:527 kwalletmanager.cpp:529 walletcontrolwidget.cpp:105
#: walletcontrolwidget.cpp:107
#, kde-format
msgid "Ignore unsaved changes?"
msgstr ""

#: kwalletmanager.cpp:527 walletcontrolwidget.cpp:105
#, kde-format
msgid "Ignore"
msgstr ""

#. i18n: ectx: Menu (file)
#: kwalletmanager.rc:4
#, kde-format
msgid "&File"
msgstr "&Dosiero"

#. i18n: ectx: Menu (settings)
#: kwalletmanager.rc:17
#, kde-format
msgid "&Settings"
msgstr "A&gordo"

#. i18n: ectx: Menu (help)
#: kwalletmanager.rc:20
#, kde-format
msgid "&Help"
msgstr "&Helpo"

#: kwalletpopup.cpp:32 walletcontrolwidget.cpp:80
#, kde-format
msgid "&Open..."
msgstr "&Malfermi..."

#: kwalletpopup.cpp:38
#, kde-format
msgid "Change &Password..."
msgstr "Ŝanĝi &pasvorton..."

#: kwalletpopup.cpp:53
#, kde-format
msgid "Disconnec&t"
msgstr ""

#: kwmapeditor.cpp:129
#, kde-format
msgid "Key"
msgstr "Klavo"

#: kwmapeditor.cpp:129
#, kde-format
msgid "Value"
msgstr "Valoro"

#: kwmapeditor.cpp:147 kwmapeditor.cpp:196
#, kde-format
msgid "Delete Entry"
msgstr "Forigi eron"

#: kwmapeditor.cpp:216
#, kde-format
msgid "&New Entry"
msgstr ""

#: main.cpp:31
#, kde-format
msgid "Wallet Manager"
msgstr ""

#: main.cpp:33
#, kde-format
msgid "KDE Wallet Management Tool"
msgstr ""

#: main.cpp:35
#, kde-format
msgid "Copyright ©2013–2017, KWallet Manager authors"
msgstr ""

#: main.cpp:39
#, kde-format
msgid "Valentin Rusu"
msgstr ""

#: main.cpp:40
#, kde-format
msgid "Maintainer, user interface refactoring"
msgstr ""

#: main.cpp:42
#, kde-format
msgid "George Staikos"
msgstr "George Staikos"

#: main.cpp:43
#, kde-format
msgid "Original author and former maintainer"
msgstr "Origina aŭtoro kaj iama vartisto"

#: main.cpp:45
#, kde-format
msgid "Michael Leupold"
msgstr "Michael Leupold"

#: main.cpp:46
#, fuzzy, kde-format
#| msgid "Original author and former maintainer"
msgid "Developer and former maintainer"
msgstr "Origina aŭtoro kaj iama vartisto"

#: main.cpp:48
#, kde-format
msgid "Isaac Clerencia"
msgstr "Isaac Clerencia"

#: main.cpp:49
#, kde-format
msgid "Developer"
msgstr "Kreinto"

#: main.cpp:57
#, kde-format
msgid "Show window on startup"
msgstr ""

#: main.cpp:58
#, kde-format
msgid "For use by kwalletd only"
msgstr ""

#: main.cpp:59
#, kde-format
msgid "A wallet name"
msgstr ""

#: revokeauthbutton.cpp:16
#, kde-format
msgid "Revoke Authorization"
msgstr ""

#: walletcontrolwidget.cpp:60
#, kde-format
msgid "&Close"
msgstr "F&ermi"

#: walletcontrolwidget.cpp:77
#, kde-format
msgctxt ""
"the 'kdewallet' is currently open (e.g. %1 will be replaced with current "
"wallet name)"
msgid "The '%1' wallet is currently open"
msgstr ""

#: walletcontrolwidget.cpp:94
#, kde-format
msgid "The wallet is currently closed"
msgstr ""

#. i18n: ectx: property (text), widget (KSqueezedTextLabel, _stateLabel)
#: walletcontrolwidget.ui:28
#, kde-format
msgid "KSqueezedTextLabel"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, _openClose)
#: walletcontrolwidget.ui:35
#, kde-format
msgid "Open..."
msgstr "Malfermi..."

#. i18n: ectx: property (text), widget (QPushButton, _changePassword)
#: walletcontrolwidget.ui:58
#, kde-format
msgid "Change Password..."
msgstr "Ŝanĝi pasvorton..."

#. i18n: ectx: attribute (title), widget (QWidget, _contentsTab)
#: walletcontrolwidget.ui:76
#, kde-format
msgid "Contents"
msgstr "Enhavo"

#. i18n: ectx: attribute (title), widget (QWidget, _appsTab)
#: walletcontrolwidget.ui:115
#, kde-format
msgid "Applications"
msgstr "Aplikaĵoj"

#. i18n: ectx: property (text), widget (QToolButton, _hideContents)
#: walletwidget.ui:142
#, kde-format
msgid "Hide &Contents"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, _binaryViewShow)
#. i18n: ectx: property (text), widget (QToolButton, _showContents)
#: walletwidget.ui:185 walletwidget.ui:233
#, kde-format
msgid "Show &Contents"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, _undoChanges)
#: walletwidget.ui:296
#, kde-format
msgid "&Undo"
msgstr "&Malfari"

#. i18n: ectx: property (text), widget (QPushButton, _saveChanges)
#: walletwidget.ui:306
#, kde-format
msgid "&Save"
msgstr "&Konservi"

#, fuzzy
#~| msgid "&New Folder..."
#~ msgid "Open Wallet..."
#~ msgstr "&Nova dosierujo..."

#, fuzzy
#~| msgid "George Staikos"
#~ msgid "(c) 2003,2004 George Staikos"
#~ msgstr "George Staikos"

#~ msgid "Maintainer"
#~ msgstr "Prizorganto"

#~ msgid "&Export..."
#~ msgstr "E&ksportu..."

#, fuzzy
#~| msgid "Change &Password..."
#~ msgid "Change password..."
#~ msgstr "Ŝanĝi &pasvorton..."
